FROM php:7.2-apache
COPY . /usr/src/realtimeedge
WORKDIR /usr/src/realtimeedge
CMD [ "php", "./realtimeedge.php" ]