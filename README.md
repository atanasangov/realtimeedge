# Prerequisites
-
PHP 7
   
# Installation instructions
- 
1. Checkout the code: 
    ````
    git clone https://atanasangov@bitbucket.org/atanasangov/realtimeedge.git
    ````
2. Navigate to the directory and run
    ````
    composer install
    ````
3. Run the tests by issuing: 
    ````
    php library/phpunit/phpunit/phpunit --configuration phpunit.xml Framework
    ```` 
4. Include the autoload.php file from the root directory of the project as shown 
in the attached file.

