<?php
/**
 * This interface is intended to be implemented by classes which can be used to print
 * the results of the rules.
 * Currently there will be only one class which implements this interface which will just
 * display the results on the standard output.
 *
 * @Note: By renaming the interface to something more generic it could be implemented by
 * more classes which could save the results into a file or even in a database.
 */

namespace realtimeedge\Framework\Output;


interface Printer
{
    public function print($data, $title);
}