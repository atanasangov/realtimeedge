<?php

namespace realtimeedge\Framework\Output;


class Standard implements Printer
{

    public function print($data, $title = '')
    {
        if(!empty($title))
        {
            print $title . PHP_EOL;
        }
        print $this->joinIfArray( $data ) . PHP_EOL;
    }

    /**
     * @param $data
     *
     * @return string
     */
    private function joinIfArray( $data ): string
    {
        if(is_array($data))
        {
            return join(PHP_EOL, $data);
        }
        return $data;
    }
}