<?php

namespace realtimeedge\Framework;

use realtimeedge\Framework\Output\Printer;
use realtimeedge\Framework\Output\Standard;
use realtimeedge\Framework\Validators\Validator;

class Rules
{
    /** @var Printer */
    private $printer;
    private $validators = [];
    private $errors = [];
    private $correct = [];

    public function __construct(Printer $printer = null)
    {
        $this->printer = $printer ?? new Standard();
    }

    public function getNumberOfValidators()
    {
        return count($this->validators);
    }

    public function addValidator( Validator $validator )
    {
        $this->validators[] = $validator;
    }

    public function runAllValidatorsBulk(array $data)
    {
        foreach($data as $datum)
        {
            if($this->runAllValidators($datum))
            {
                $this->correct[] = $datum;
            }
        }
        $this->printResults();
    }

    /**
     * @param string $data The data to run the validators against
     *
     * @return bool
     */
    private function runAllValidators($data)
    {
        /** @var Validator $validator */
        foreach($this->validators as $validator)
        {
            if(!$validator->validate($data))
            {
                $this->errors[$data][] = $validator->getMessage($data);
            }
        }
        return empty($this->errors[$data]);
    }

    private function printResults()
    {
        $this->printer->print($this->correct, 'Correct emails:');
        $this->printer->print(PHP_EOL . 'Wrong emails: ');
        foreach($this->errors as $email => $errors)
        {
            $this->printer->print($errors, '');
        }
    }
}