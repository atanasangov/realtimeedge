<?php

namespace realtimeedge\Framework\Validators;


use PHPUnit_Framework_TestCase;

class BlacklistedUserPartValidatorTest extends PHPUnit_Framework_TestCase
{
    /** @var BlacklistedUserPartValidator */
    protected $validator;

    protected function setUp()
    {
        $this->validator = new BlacklistedUserPartValidator();
    }

    public function testValidateSuccess()
    {
        $this->assertTrue($this->validator->validate('correctemail@gmail.com'));
    }

    public function testValidateBlacklisted()
    {
        $this->assertFalse($this->validator->validate('badword1@gmail.com'));
    }

}