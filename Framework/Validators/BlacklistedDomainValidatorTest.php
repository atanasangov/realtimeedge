<?php

namespace realtimeedge\Framework\Validators;


use PHPUnit_Framework_TestCase;

class BlacklistedDomainValidatorTest extends PHPUnit_Framework_TestCase
{
    /** @var Validator */
    private $validator;

    protected function setUp()
    {
        $this->validator = new BlacklistedDomainValidator();
    }

    public function testValidateSuccess()
    {
        $this->assertTrue($this->validator->validate('correctemail@gmail.com'));
    }

    public function testValidateBlacklisted()
    {
        $this->assertFalse($this->validator->validate('email@baddomain1.com'));
    }
}