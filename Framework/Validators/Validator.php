<?php

namespace realtimeedge\Framework\Validators;


interface Validator
{
    public function validate($data);
    public function getMessage($data);
}