<?php

namespace realtimeedge\Framework\Validators;


class BlacklistedDomainValidator implements Validator
{
    private $blacklistedDomains = [
        'baddomain1.com',
        'baddomain2.com',
        'baddomain3.com',
        'baddomain4.com',
        'baddomain5.com',
    ];

    public function validate($email)
    {
        $domain = substr(strrchr($email, "@"), 1);
        return !in_array($domain, $this->blacklistedDomains);
    }

    public function getMessage($data)
    {
        return "$data contains a blacklisted domain part.";
    }
}