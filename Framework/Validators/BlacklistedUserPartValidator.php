<?php

namespace realtimeedge\Framework\Validators;


class BlacklistedUserPartValidator implements Validator
{
    private $blacklistedUserParts = [
        'badword1',
        'badword2',
        'badword3',
        'badword4',
        'badword5',
    ];

    public function validate($email)
    {
        $userPart = substr($email, 0, strpos($email, '@'));
        return !in_array($userPart, $this->blacklistedUserParts);
    }

    public function getMessage($data)
    {
        return "$data contains a blacklisted user part.";
    }
}