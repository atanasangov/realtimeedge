<?php

namespace realtimeedge\Framework;

use PHPUnit_Framework_TestCase;
use realtimeedge\Framework\Validators\Validator;

class RulesTest extends PHPUnit_Framework_TestCase
{
    public function testInit()
    {
        $rules = new Rules();
        $this->assertEquals(0, $rules->getNumberOfValidators());
    }

    public function testAddRule()
    {
        $rules = new Rules();
        $rules->addValidator( new class implements Validator {

            public function validate( $data )
            {
            }

            public function getMessage( $data )
            {
            }
        });

        $this->assertEquals(1, $rules->getNumberOfValidators());
    }
}